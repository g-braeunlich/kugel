mod graphics;

use minifb::{Key, Window, WindowOptions};

use crate::graphics::{raqote::Canvas, svg};
use kugel_core::{kernel::Kernel, render::Render};

const WIDTH: usize = 400;
const HEIGHT: usize = 400;
const TIME_STEP: f64 = 0.016666666666666666;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut app = App::new(load_kernel(std::env::args().nth(1))?);
    let (_x_min, x_max, _y_min, y_max) = app
        .kernel
        .boundary
        .boundary_box()
        .ok_or("Configutation witout boundary!")?;
    let mut window = Window::new(
        "kugel",
        x_max as usize,
        y_max as usize,
        WindowOptions {
            ..WindowOptions::default()
        },
    )?;
    // Limit to max update rate
    window.set_target_fps(1_000_000 / app.dt as usize);
    let (w, h) = window.get_size();
    let mut canvas = Canvas::new(w as f64, h as f64);
    while window.is_open() && !window.is_key_down(Key::Escape) && !window.is_key_down(Key::Q) {
        if window.is_key_released(Key::Space) {
            app.pause = !app.pause;
        }
        if window.is_key_released(Key::Up) {
            app.increase_framerate();
        }
        if window.is_key_released(Key::Down) {
            app.decrease_framerate();
        }
        if window.is_key_released(Key::R) {
            app.toggle_record();
        }
        if window.is_key_released(Key::P) {
            app.screenshot();
        }
        if !app.pause {
            app.step();
            app.kernel.render(&mut canvas);
            window.update_with_buffer(canvas.get_data(), w, h)?;
        } else {
            window.update();
        }
        std::thread::sleep(std::time::Duration::from_micros(app.dt));
    }
    Ok(())
}

struct App {
    kernel: Kernel,
    record_buffer: Option<kugel_core::trajectory::Trajectory>,
    pub pause: bool,
    /// sleep time [µs]
    pub dt: u64,
}

fn load_kernel<T: AsRef<std::path::Path>>(
    filename_arg: Option<T>,
) -> Result<Kernel, Box<dyn std::error::Error>> {
    Ok(match filename_arg {
        Some(filename) => {
            let file = std::fs::File::open(filename)?;
            let reader = std::io::BufReader::new(file);
            serde_json::from_reader(reader)?
        }
        None => Kernel::new(WIDTH as f64, HEIGHT as f64, 20),
    })
}

impl App {
    fn new(core: Kernel) -> Self {
        App {
            kernel: core,
            record_buffer: None,
            pause: false,
            dt: 16666, // ~60 fps
        }
    }
    fn increase_framerate(&mut self) {
        self.dt /= 2;
    }
    fn decrease_framerate(&mut self) {
        self.dt *= 2;
    }
    fn step(&mut self) {
        self.kernel.step(TIME_STEP);
        if let Some(buffer) = &mut self.record_buffer {
            buffer.take_snapshot(&self.kernel.objects);
        }
    }
    fn toggle_record(&mut self) {
        if let Some(buffer) = &self.record_buffer {
            let bbox = self.kernel.boundary.boundary_box().unwrap();
            svg::save_to_svg(buffer, screenshot_file_name(), &bbox).unwrap();
            self.record_buffer = None;
        } else {
            self.record_buffer = Some(kugel_core::trajectory::Trajectory::new());
        }
    }
    fn screenshot(&self) {
        let bbox = self.kernel.boundary.boundary_box().unwrap();
        svg::save_to_svg(&self.kernel, screenshot_file_name(), &bbox).unwrap();
    }
}

fn now_str() -> impl std::fmt::Display {
    use chrono::Local;
    Local::now().format("%Y-%m-%dT%H%M%S")
}
fn screenshot_file_name() -> std::path::PathBuf {
    let mut path = std::env::temp_dir();
    path.push(format!("kugel-{}.svg", now_str()));
    path
}
