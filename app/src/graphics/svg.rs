//! svg graphics backend

use kugel_core::render::Render;
use svg::node::element::path::Data;
use svg::node::{
    element::{Circle, Path, Rectangle, Text},
    Node, Value,
};

use kugel_core::graphics;

/// Save an object which implements [Render] to a svg file
pub fn save_to_svg<O, T: AsRef<std::path::Path>>(
    object: &O,
    file_name: T,
    boundary_box: &(f64, f64, f64, f64),
) -> Result<(), std::io::Error>
where
    for<'a> &'a O: Render,
{
    let (x_min, x_max, y_min, y_max) = boundary_box;
    let mut svg = Canvas::new(*x_min, *y_min, *x_max - *x_min, *y_max - *y_min);
    object.render(&mut svg);
    svg.save(file_name)
}

/// svg canvas implementing [graphics::Canvas]
pub struct Canvas {
    doc: svg::Document,
}

impl Default for Canvas {
    fn default() -> Self {
        Self {
            doc: svg::Document::new(),
        }
    }
}

/// Convert a [graphics::Color] to a web color
fn to_svg_color(clr: &graphics::Color) -> String {
    format!("#{:06X}", u32::from(clr) >> 8)
}

impl Canvas {
    /// Create a new svg canvas for a given bounding box
    pub fn new(x: f64, y: f64, w: f64, h: f64) -> Self {
        Self {
            doc: svg::Document::new().set("viewBox", (x, y, w, h)),
        }
    }
    /// Save the canvas to a svg file
    pub fn save<T: AsRef<std::path::Path>>(&self, filename: T) -> Result<(), std::io::Error> {
        svg::save(filename, &self.doc)
    }
    /// Add a svg element to the canvas
    fn add<T: Node>(&mut self, node: T) {
        self.doc = std::mem::take(self).doc.add(node);
    }
}

/// Helper trait to shorten expressions when building svg elements
trait ModifyNode: Sized {
    fn set<T: Into<String>, U: Into<Value>>(self, name: T, value: U) -> Self;
    fn set_stroke(self, line_style: &graphics::LineStyle) -> Self {
        let obj = self
            .set("stroke-width", line_style.width)
            .set(
                "stroke-dasharray",
                line_style
                    .dash_lengths
                    .iter()
                    .map(|x| format!("{}", x))
                    .collect::<Vec<String>>()
                    .join(" "),
            )
            .set("stroke-dashoffset", line_style.dash_offset)
            .set("fill", "none")
            .set("stroke", to_svg_color(&line_style.color));
        if line_style.color.a == 1.0 {
            obj
        } else {
            obj.set("stroke-opacity", line_style.color.a)
        }
    }
    fn set_fill(self, color: &graphics::Color) -> Self {
        let obj = self.set("fill", to_svg_color(color));
        if color.a == 1.0 {
            obj
        } else {
            obj.set("fill-opacity", color.a)
        }
    }
}

impl ModifyNode for Rectangle {
    fn set<T: Into<String>, U: Into<Value>>(self, name: T, value: U) -> Self {
        self.set(name, value)
    }
}
impl ModifyNode for Path {
    fn set<T: Into<String>, U: Into<Value>>(self, name: T, value: U) -> Self {
        self.set(name, value)
    }
}
impl ModifyNode for Circle {
    fn set<T: Into<String>, U: Into<Value>>(self, name: T, value: U) -> Self {
        self.set(name, value)
    }
}

fn polygon_path<'a>(p: impl IntoIterator<Item = &'a (f64, f64)>) -> Data {
    let data = Data::new();
    let mut iter = p.into_iter();
    if let Some(p) = iter.next() {
        iter.fold(data.move_to(*p), |d, p| d.line_to(*p))
    } else {
        data
    }
}

fn rect_path(x: f64, y: f64, w: f64, h: f64) -> Rectangle {
    Rectangle::new()
        .set("x", x)
        .set("y", y)
        .set("width", w)
        .set("height", h)
}

fn circle_path(x: f64, y: f64, r: f64) -> Circle {
    Circle::new().set("cx", x).set("cy", y).set("r", r)
}

impl graphics::StrokeRectangle for Canvas {
    fn stroke_rectangle(
        &mut self,
        x: f64,
        y: f64,
        w: f64,
        h: f64,
        line_style: &graphics::LineStyle,
    ) {
        self.add(rect_path(x, y, w, h).set_stroke(line_style));
    }
}
impl graphics::FillRectangle for Canvas {
    fn fill_rectangle(&mut self, x: f64, y: f64, w: f64, h: f64, color: &graphics::Color) {
        self.add(rect_path(x, y, w, h).set_fill(color));
    }
}
impl graphics::StrokePolygon for Canvas {
    fn stroke_polygon(&mut self, p: &[(f64, f64)], line_style: &graphics::LineStyle) {
        self.add(
            Path::new()
                .set("d", polygon_path(p).close())
                .set_stroke(line_style),
        );
    }
}
impl graphics::StrokePath for Canvas {
    fn stroke_path<'a>(
        &mut self,
        p: impl IntoIterator<Item = &'a (f64, f64)>,
        line_style: &graphics::LineStyle,
    ) {
        self.add(Path::new().set("d", polygon_path(p)).set_stroke(line_style));
    }
}
impl graphics::FillPolygon for Canvas {
    fn fill_polygon(&mut self, p: &[(f64, f64)], color: &graphics::Color) {
        self.add(
            Path::new()
                .set("d", polygon_path(p).close())
                .set_fill(color),
        );
    }
}
impl graphics::FillCircle for Canvas {
    fn fill_circle(&mut self, x: f64, y: f64, r: f64, color: &graphics::Color) {
        self.add(circle_path(x, y, r).set_fill(color));
    }
}
impl graphics::StrokeCircle for Canvas {
    fn stroke_circle(&mut self, x: f64, y: f64, r: f64, line_style: &graphics::LineStyle) {
        self.add(circle_path(x, y, r).set_stroke(line_style));
    }
}
impl graphics::DrawPoint for Canvas {
    fn draw_point(&mut self, x: f64, y: f64, color: &graphics::Color) {
        use graphics::FillRectangle;
        self.fill_rectangle(x, y, 1., 1., color)
    }
}
impl graphics::DrawTextCentered for Canvas {
    fn draw_text_centered(&mut self, x: f64, y: f64, text: &str, font: &graphics::Font) {
        self.add(
            Text::new(text)
                .set("dominant-baseline", "middle")
                .set(
                    "style",
                    format!(
                        "font-weight:{};font-style:{};font-family:{};font-size:{};fill:{};text-anchor:middle;text-align:center",
                        match font.weight {
                            graphics::FontWeight::Normal => "normal",
                            graphics::FontWeight::Bold => "bold",
                        },
                        match font.slant {
                            graphics::FontSlant::Normal => "normal",
                            graphics::FontSlant::Italic => "italic",
                            graphics::FontSlant::Oblique => "oblique",
                        },
                        font.family,
                        font.size,
                        to_svg_color(&font.color)
                    ),
                )
                .set("x", x)
                .set("y", y)
        );
    }
}
