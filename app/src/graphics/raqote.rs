//! ui graphics backend

use raqote::{DrawOptions, PathBuilder, Point, SolidSource, Source, StrokeStyle};

use kugel_core::graphics;

/// ui canvas implementing [crate::graphics::Canvas]
pub struct Canvas(raqote::DrawTarget);

impl Canvas {
    /// Create a new raqote canvas for given width and height
    pub fn new(w: f64, h: f64) -> Self {
        Self(raqote::DrawTarget::new(w as i32, h as i32))
    }
    /// Access the bytes stored in the canvas
    pub fn get_data(&self) -> &[u32] {
        self.0.get_data()
    }
}

fn into_solid_source(c: &graphics::Color) -> SolidSource {
    SolidSource::from_unpremultiplied_argb(
        (0xff as f64 * c.a) as u8,
        (0xff as f64 * c.r) as u8,
        (0xff as f64 * c.g) as u8,
        (0xff as f64 * c.b) as u8,
    )
}

fn into_stroke_style(line_style: &graphics::LineStyle) -> StrokeStyle {
    StrokeStyle {
        cap: raqote::LineCap::Square,
        join: raqote::LineJoin::Miter,
        miter_limit: 0.,
        width: line_style.width as f32,
        dash_array: line_style.dash_lengths.iter().map(|l| *l as f32).collect(),
        dash_offset: line_style.dash_offset as f32,
    }
}

fn polygon_path<'a>(p: impl IntoIterator<Item = &'a (f64, f64)>) -> PathBuilder {
    let mut pb = PathBuilder::new();
    let mut iter = p.into_iter();
    if let Some((x, y)) = iter.next() {
        pb.move_to(*x as f32, *y as f32);
    } else {
        return pb;
    }
    for (x, y) in iter {
        pb.line_to(*x as f32, *y as f32);
    }
    pb
}

fn circle_path(x: f64, y: f64, r: f64) -> raqote::Path {
    let mut pb = PathBuilder::new();
    pb.arc(x as f32, y as f32, r as f32, 0., 2. * std::f32::consts::PI);
    pb.close();
    pb.finish()
}

impl graphics::StrokeRectangle for Canvas {
    fn stroke_rectangle(
        &mut self,
        x: f64,
        y: f64,
        w: f64,
        h: f64,
        line_style: &graphics::LineStyle,
    ) {
        let mut pb = PathBuilder::new();
        pb.rect(x as f32, y as f32, w as f32, h as f32);
        self.0.stroke(
            &pb.finish(),
            &Source::Solid(into_solid_source(&line_style.color)),
            &into_stroke_style(line_style),
            &DrawOptions::new(),
        );
    }
}
impl graphics::FillRectangle for Canvas {
    fn fill_rectangle(&mut self, x: f64, y: f64, w: f64, h: f64, color: &graphics::Color) {
        self.0.fill_rect(
            x as f32,
            y as f32,
            w as f32,
            h as f32,
            &Source::Solid(into_solid_source(color)),
            &DrawOptions::new(),
        );
    }
}
impl graphics::StrokePolygon for Canvas {
    fn stroke_polygon(&mut self, p: &[(f64, f64)], line_style: &graphics::LineStyle) {
        let mut pb = polygon_path(p);
        pb.close();
        self.0.stroke(
            &pb.finish(),
            &Source::Solid(into_solid_source(&line_style.color)),
            &into_stroke_style(line_style),
            &DrawOptions::new(),
        );
    }
}
impl graphics::StrokePath for Canvas {
    fn stroke_path<'a>(
        &mut self,
        p: impl IntoIterator<Item = &'a (f64, f64)>,
        line_style: &graphics::LineStyle,
    ) {
        self.0.stroke(
            &polygon_path(p).finish(),
            &Source::Solid(into_solid_source(&line_style.color)),
            &into_stroke_style(line_style),
            &DrawOptions::new(),
        );
    }
}
impl graphics::FillPolygon for Canvas {
    fn fill_polygon(&mut self, p: &[(f64, f64)], color: &graphics::Color) {
        let mut pb = polygon_path(p);
        pb.close();
        self.0.fill(
            &pb.finish(),
            &Source::Solid(into_solid_source(color)),
            &DrawOptions::new(),
        );
    }
}
impl graphics::FillCircle for Canvas {
    fn fill_circle(&mut self, x: f64, y: f64, r: f64, color: &graphics::Color) {
        self.0.fill(
            &circle_path(x, y, r),
            &Source::Solid(into_solid_source(color)),
            &DrawOptions::new(),
        );
    }
}
impl graphics::StrokeCircle for Canvas {
    fn stroke_circle(&mut self, x: f64, y: f64, r: f64, line_style: &graphics::LineStyle) {
        self.0.stroke(
            &circle_path(x, y, r),
            &Source::Solid(into_solid_source(&line_style.color)),
            &into_stroke_style(line_style),
            &DrawOptions::new(),
        );
    }
}
impl graphics::DrawPoint for Canvas {
    fn draw_point(&mut self, x: f64, y: f64, color: &graphics::Color) {
        use graphics::FillRectangle;
        self.fill_rectangle(x, y, 1., 1., color)
    }
}
impl graphics::DrawTextCentered for Canvas {
    fn draw_text_centered(&mut self, x: f64, y: f64, text: &str, font: &graphics::Font) {
        let rq_font = font_kit::source::SystemSource::new()
            .select_best_match(
                &[font_kit::family_name::FamilyName::SansSerif],
                &font_kit::properties::Properties::new(),
            )
            .unwrap()
            .load()
            .unwrap();
        self.0.draw_text(
            &rq_font,
            font.size as f32,
            text,
            Point::new(x as f32, y as f32),
            &Source::Solid(SolidSource::from_unpremultiplied_argb(
                (0xff as f64 * font.color.r) as u8,
                (0xff as f64 * font.color.g) as u8,
                (0xff as f64 * font.color.b) as u8,
                (0xff as f64 * font.color.a) as u8,
            )),
            &DrawOptions::new(),
        );
    }
}
