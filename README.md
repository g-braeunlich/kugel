<div align="center">

A small physics simulation of balls / rectangles confined within
boundary walls, optionally interacting via a central force.
The central force can behave like gravity or the electric force
according to the sign of the force strength constant. A Lorentz force
is currently not implemented.

[![pipeline status](https://gitlab.com/g-braeunlich/kugel/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/g-braeunlich/kugel/-/commits/main)
[![Checked with clippy](https://img.shields.io/badge/clippy-checked-blue)](https://doc.rust-lang.org/stable/clippy/usage.html)
[![License](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

![kugel](doc/kugel.svg)


**[Install](#install)** •
**[Usage](#usage)** •
**[Develop](#develop)** •
**[Mathematical model](#mathematical-model)**

# Kugel

</div>

⚠️: The numerical integration of central forces becomes inaccurate if the time step is to large (in this simulation the time step is fixed, but the strength of the central force can be modified). If the forces become too strong, expect unphysical behaviour.

This is a 🦀 (rust) port of [gitlab.com/g-braeunlich/kugel-cpp](https://gitlab.com/g-braeunlich/kugel-cpp).

## Gallery

[Live version](https://g-braeunlich.gitlab.io/kugel) (or click on the gallery)

[![gallery](doc/gallery.svg)](https://g-braeunlich.gitlab.io/kugel)

## Install

Latest binary builds

| 🐧 | Windows |
|----|---------|
| [📥](../-/jobs/artifacts/master/raw/target/release/kugel?job=bin) | [📥](../-/jobs/artifacts/master/raw/target/x86_64-pc-windows-gnu/release/kugel.exe?job=exe) |

The binaries are statically linked and should not require
any dependencies.

## Usage

Just run the binary `kugel`. Either with a single argument (a `json`
file, see [examples](examples)) or without. During the simulation, the following key
combinations are available:

| Key          | Action                                       |
|--------------|----------------------------------------------|
| `Q` or `Esc` | Quit                                         |
| `Space`      | Pause                                        |
| `P`          | Take a svg screenshot                        |
| `R`          | Start / stop recording motion trace to a svg |
| `↑`          | Increase frame rate                          |
| `↓`          | Decrease frame rate                          |

The screenshots / motion traces are stored in files
`/tmp/kugel-YYYY-mm-ddTHHMMSS.svg` (on 🪟 files are stored in the os
specific temporary directory).

## Build from source

```bash
cargo build --release
```

## Develop

### Run unit tests

```bash
cargo build test
```

## Mathematical model

For a detailed description, see the 
[doc](https://g-braeunlich.gitlab.io/kugel/physics.html)


## Credits

This project originated around 2001, when our math teacher, Peter
Strebel introduced us to java. A year later, I continued work on the inital idea in my Matura
Thesis under his supervision.
