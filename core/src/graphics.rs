//! Abstraction traits for graphics backends (svg/ui) and graphics structs

use serde::Deserialize;

/// Abstraction of stroking a rectangle
pub trait StrokeRectangle {
    fn stroke_rectangle(&mut self, x: f64, y: f64, w: f64, h: f64, line_style: &LineStyle);
}

/// Abstraction of filling a rectangle
pub trait FillRectangle {
    fn fill_rectangle(&mut self, x: f64, y: f64, w: f64, h: f64, color: &Color);
}

/// Abstraction of stroking a circle
pub trait StrokeCircle {
    fn stroke_circle(&mut self, x: f64, y: f64, r: f64, line_style: &LineStyle);
}

/// Abstraction of filling a circle
pub trait FillCircle {
    fn fill_circle(&mut self, x: f64, y: f64, r: f64, color: &Color);
}

/// Abstraction of stroking a polygon
pub trait StrokePolygon {
    fn stroke_polygon(&mut self, p: &[(f64, f64)], style: &LineStyle);
}

/// Abstraction of stroking a polygonal chain
pub trait StrokePath {
    fn stroke_path<'a>(&mut self, p: impl IntoIterator<Item = &'a (f64, f64)>, style: &LineStyle);
}

/// Abstraction of filling a polygon
pub trait FillPolygon {
    fn fill_polygon(&mut self, p: &[(f64, f64)], color: &Color);
}

/// Abstraction of drawing a point
pub trait DrawPoint {
    fn draw_point(&mut self, x: f64, y: f64, color: &Color);
}

/// Abstraction of drawing centered text
pub trait DrawTextCentered {
    fn draw_text_centered(&mut self, x: f64, y: f64, text: &str, font: &Font);
}

/// Abstraction of all graphical instructions needed for the simulation
pub trait Canvas:
    StrokeRectangle
    + FillRectangle
    + StrokeCircle
    + FillCircle
    + StrokePolygon
    + StrokePath
    + FillPolygon
    + DrawPoint
    + DrawTextCentered
{
}

impl<
        T: StrokeRectangle
            + FillRectangle
            + StrokeCircle
            + FillCircle
            + StrokePolygon
            + StrokePath
            + FillPolygon
            + DrawPoint
            + DrawTextCentered,
    > Canvas for T
{
}

/// Representation of a color
#[derive(Debug, Clone, Default, Deserialize)]
#[serde(try_from = "String")]
pub struct Color {
    pub r: f64,
    pub g: f64,
    pub b: f64,
    pub a: f64,
}
impl std::convert::From<&Color> for u32 {
    fn from(clr: &Color) -> Self {
        u32::from_be_bytes([
            (255. * clr.r) as u8,
            (255. * clr.g) as u8,
            (255. * clr.b) as u8,
            (255. * clr.a) as u8,
        ])
    }
}

impl std::convert::From<&Color> for String {
    fn from(clr: &Color) -> Self {
        let mut bytes = u32::from(clr);
        if bytes & 0xFF == 0xFF {
            bytes >>= 8;
            format!("#{:06X}", bytes)
        } else {
            format!("#{:08X}", bytes)
        }
    }
}

impl std::convert::TryFrom<&str> for Color {
    type Error = String;
    fn try_from(other: &str) -> Result<Self, Self::Error> {
        let bytes = other
            .get(..1)
            .filter(|s| s == &"#" && other.len() >= 7)
            .and(other.get(1..))
            .and_then(|s| u32::from_str_radix(s, 16).ok())
            .map(|b| if other.len() == 7 { (b << 8) + 0xFF } else { b })
            .ok_or(format!("Invalid color: {}", other))?;
        let [r, g, b, a] = bytes.to_be_bytes();
        Ok(Color {
            r: r as f64 / 255.,
            g: g as f64 / 255.,
            b: b as f64 / 255.,
            a: a as f64 / 255.,
        })
    }
}
impl std::convert::TryFrom<String> for Color {
    type Error = String;
    fn try_from(other: String) -> Result<Self, Self::Error> {
        Self::try_from(&other as &str)
    }
}

impl Color {
    pub fn get_lightness(&self) -> f64 {
        f64::max(f64::max(self.r, self.g), self.b)
    }
    pub const WHITE: Color = Color {
        r: 1.,
        g: 1.,
        b: 1.,
        a: 1.,
    };
    pub const BLACK: Color = Color {
        r: 0.,
        g: 0.,
        b: 0.,
        a: 1.,
    };
}

/// Representation of a line style
pub struct LineStyle {
    pub width: f64,
    pub color: Color,
    pub dash_lengths: Vec<f64>,
    pub dash_offset: f64,
}

pub const DEFAULT_LINE_STYLE: LineStyle = LineStyle {
    width: 1.,
    color: Color::BLACK,
    dash_lengths: Vec::new(),
    dash_offset: 0.,
};

/// Representation of a font slant
pub enum FontSlant {
    Normal,
    Italic,
    Oblique,
}

/// Representation of a font weight
pub enum FontWeight {
    Normal,
    Bold,
}

/// Representation of a font
pub struct Font<'a> {
    pub slant: FontSlant,
    pub weight: FontWeight,
    pub family: &'a str,
    pub size: f64,
    pub color: Color,
}
