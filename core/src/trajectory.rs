#[derive(Default)]
/// Record trajectories from moving objects
pub struct Trajectory {
    pub snapshots: Vec<Vec<crate::objects::Body>>,
}

impl Trajectory {
    /// Create an empty trajectory
    pub fn new() -> Self {
        Trajectory {
            ..Default::default()
        }
    }
    /// Track trajectories from an animation
    pub fn track(mut kernel: crate::kernel::Kernel, t_start: f64, t_stop: f64, dt: f64) -> Self {
        kernel.step(t_start);
        let mut t = t_start;
        let mut trajectory = Self::new();
        while t < t_stop {
            trajectory.take_snapshot(&kernel.objects);
            kernel.step(dt);
            t += dt;
        }
        trajectory
    }
    /// Take a snapshot, removing objects in the previous snapshot if they did not move.
    pub fn take_snapshot(&mut self, objects: &[crate::objects::Body]) {
        if let Some(snapshot) = self.snapshots.last_mut() {
            let remove_indices: Vec<usize> = snapshot
                .iter()
                .enumerate()
                .rev()
                .filter_map(|(i, obj)| {
                    if obj.vx == 0. && obj.vy == 0. && obj.ax == 0. && obj.ay == 0. {
                        Some(i)
                    } else {
                        None
                    }
                })
                .collect();
            for i in remove_indices {
                snapshot.remove(i);
            }
        }
        self.snapshots.push(objects.to_vec());
    }
}

const ALPHA_MULTIPLIER: f64 = 0.9;
impl crate::render::Render for &Trajectory {
    fn render(self, canvas: &mut impl crate::graphics::Canvas) {
        let alphas: Vec<f64> = std::iter::successors(Some(1.), |a| Some(a * ALPHA_MULTIPLIER))
            .take(self.snapshots.len())
            .collect();
        for (alpha, snapshot) in alphas.into_iter().rev().zip(&self.snapshots) {
            for object in snapshot.iter() {
                crate::render::WithAlpha { object, alpha }.render(canvas);
            }
        }
    }
}
