#![allow(non_snake_case)]

const N: usize = 100;
const UNDEFINED: f64 = -2.;

///  Find smallest real zero x0 of
///  p(x) = \sum_{n=0}^4 a[n] x^n
///  such that
///  x0 >= 0 and p'(x0) <= 0
pub fn zero_p4(a: &[f64; 5], eps: f64) -> f64 {
    // Part 1: Factorize p(x) ~ (x^2 + q x + r)(x^2 + q2 x + r2)
    let mut q = a[3] / a[4];
    let mut r = a[2] / a[4];
    let mut temp: f64;
    let mut temp2: f64;
    for _ in 0..N {
        let d1 = a[1] + (q * q - r) * a[3] + q * (2. * r - q * q) * a[4] - q * a[2];
        let d2 = a[0] - r * a[2] - r * (q * q - r) * a[4] + r * q * a[3];
        if f64::abs(d1) < eps && f64::abs(d2) < eps {
            break;
        }
        let det = r * (-a[3] + 2. * q * a[4]) * (-a[3] + 2. * q * a[4])
            + (-a[2] + 2. * q * a[3] + (2. * r - 3. * q * q) * a[4])
                * (-a[2] + q * a[3] + (2. * r - q * q) * a[4]);
        // Newton:
        // d(x0) + Dd(x0)(x-x0) = 0 => x = x0 - Dd^-1 d(x0)
        temp =
            ((-a[2] + (2. * r - q * q) * a[4] + q * a[3]) * d1 + (a[3] - 2. * q * a[4]) * d2) / det;
        r -= ((2. * r * q * a[4] - r * a[3]) * d1
            + (-a[2] + 2. * q * a[3] + (2. * r - 3. * q * q) * a[4]) * d2)
            / det;
        q -= temp;
    }
    // Part 2: Examine zeros
    let mut D = q * q - 4. * r;
    // [-b -s*sqrt(b^2 - 4ac)]/(2a) = 2c/[-b +s*sqrt(b^2 - 4ac)]
    // p'(x_0) <= 0
    // p(x) = p_1(x) p_2(x)
    // p_1(x_0) = 0 => p'(x_0) = p_1'(x_0) p_2(x_0)
    let mut t = UNDEFINED;
    let q2 = a[3] / a[4] - q;
    let r2 = a[2] / a[4] + (q * q - r) - q * a[3] / a[4];
    if D >= 0. {
        temp = -q - D.sqrt().copysign(q);
        temp2 = 2. * r / temp;
        temp *= 0.5;
        if temp >= -eps && (2. * temp + q) * (temp * (temp + q2) + r2) <= 0. {
            t = temp;
        }
        if -eps <= temp2
            && (temp2 < t || t < 0.)
            && (2. * temp2 + q) * (temp2 * (temp2 + q2) + r2) <= 0.
        {
            t = temp2;
        }
    }
    D = q2 * q2 - 4. * r2;
    if D >= 0. {
        temp = -q2 - D.sqrt().copysign(q2);
        temp2 = 2. * r2 / temp;
        temp *= 0.5;
        if -eps <= temp && (temp < t || t < 0.) && (2. * temp + q2) * (temp * (temp + q) + r) <= 0.
        {
            t = temp;
        }
        if -eps <= temp2
            && (temp2 < t || t < 0.)
            && (2. * temp2 + q2) * (temp2 * (temp2 + q) + r) <= 0.
        {
            t = temp2;
        }
    }
    t
}

#[cfg(test)]
mod tests {
    use crate::numerics::*;
    const EPS_EQ: f64 = 0.5E-10;
    const EPS: f64 = 1.0E-10;

    macro_rules! assert_eq_delta {
        ($x:expr, $y:expr, $d: expr) => {
            if (f64::is_nan($x) ^ f64::is_nan($y)) || ($x - $y).abs() > $d {
                panic!(
                    "assertion failed: `(left == right)`\n  left: `{}`\n right: `{}`",
                    $x, $y
                );
            }
        };
    }
    #[macro_export]
    macro_rules! assert_almost_eq {
        ($x:expr, $y:expr) => {
            assert_eq_delta!($x, $y, EPS_EQ);
        };
    }

    /// p(x) = (x + 1)(x - 1)(x - 2)(x - 3) = (x^2-1)(x^2-5x+6)
    ///      = x^4 - 5x^3 + 5x^2 + 5x - 6
    #[test]
    fn test_zero_p4_1() {
        let p = [-6., 5., 5., -5., 1.];
        let t = zero_p4(&p, EPS);
        assert_almost_eq!(t, 2.);
    }

    /// p(x) = (x + 1)(x - 1)^2(x - 2) = (x^2-1)(x^2-3x+2)
    ///      = x^4 - 3x^3 + x^2 + 3x - 2
    #[test]
    fn test_zero_p4_2() {
        let p = [-2., 3., 1., -3., 1.];
        let t = zero_p4(&p, EPS);
        assert_almost_eq!(t, 1.);
    }

    /// p(x) = (x^2 + 1)(x - 1)(x - 2) = (x^2+1)(x^2-3x+2)
    ///      = x^4 - 3x^3 + 3x^2 - 3x + 2
    #[test]
    fn test_zero_p4_3() {
        let p = [2., -3., 3., -3., 1.];
        let t = zero_p4(&p, EPS);
        assert_almost_eq!(t, 1.);
    }

    /// p(x) = (x^2 + 1)(x^2 + 2)
    ///      = x^4 + 3x^2 + 2
    #[test]
    fn test_zero_p4_4() {
        let p = [2., 0., 3., 0., 1.];
        let t = zero_p4(&p, EPS);
        assert_almost_eq!(t, UNDEFINED);
    }
}
