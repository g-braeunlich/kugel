use crate::kernel::ObjectContainer;
use crate::objects::{AbstractBody, Ball, Body, ObjectType};
use serde::{Deserialize, Deserializer};
use std::f64::consts::PI;

pub trait DecayEffect: clone_inner::CloneDecayEffect {
    fn execute(&self, objects: &mut ObjectContainer, obj: usize);
}

pub trait CollisionEffect: clone_inner::CloneCollisionEffect {
    fn execute(&self, objects: &mut ObjectContainer, obj_1: usize, obj_2: usize);
    fn decompose(&self) -> [Option<Box<dyn DecayEffect>>; 2];
    fn swapped(&self) -> Box<dyn CollisionEffect>;
}

#[derive(Deserialize)]
#[serde(tag = "__name__")]
#[serde(rename_all = "snake_case")]
enum CollisionEffects {
    DisentangledCollisionEffect(DisentangledCollisionEffect),
    Merge(Merge),
}
#[derive(Debug, Deserialize)]
#[serde(tag = "__name__")]
#[serde(rename_all = "snake_case")]
enum DecayEffects {
    Destroy(Destroy),
    #[serde(rename = "explosion-ring")]
    ExplosionRing(ExplosionRing),
    Explosion(Explosion),
}

impl From<DecayEffects> for Box<dyn DecayEffect> {
    fn from(other: DecayEffects) -> Self {
        match other {
            DecayEffects::Destroy(eff) => Box::new(eff),
            DecayEffects::ExplosionRing(eff) => Box::new(eff),
            DecayEffects::Explosion(eff) => Box::new(eff),
        }
    }
}
impl<'de> Deserialize<'de> for Box<dyn DecayEffect> {
    fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        DecayEffects::deserialize(deserializer).map(<DecayEffects>::into)
    }
}
impl<'de> Deserialize<'de> for Box<dyn CollisionEffect> {
    fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        CollisionEffects::deserialize(deserializer).map(<CollisionEffects>::into)
    }
}
/*
trait DeserializeFrom<'de>: std::marker::Sized {
    type Via: Into<Self> + Deserialize<'de>;
}
impl<'de> DeserializeFrom<'de> for Box<dyn DecayEffect> {
    type Via = DecayEffects;
}

impl<'de, T> Deserialize<'de> for T
where
    T: DeserializeFrom<'de>,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        T::deserialize(deserializer).map(T::into)
    }
}
*/
impl From<CollisionEffects> for Box<dyn CollisionEffect> {
    fn from(other: CollisionEffects) -> Self {
        match other {
            CollisionEffects::Merge(eff) => Box::new(eff),
            CollisionEffects::DisentangledCollisionEffect(eff) => Box::new(eff),
        }
    }
}

#[derive(Clone, Debug, Deserialize)]
struct Destroy {}
impl DecayEffect for Destroy {
    fn execute(&self, objects: &mut ObjectContainer, obj: usize) {
        objects.remove(obj);
    }
}
#[derive(Clone, Debug, Deserialize)]
struct ExplosionRing {
    n: u32,
    phi: f64,
    tau: f64,
    r: f64,
    substantial: bool,
}
impl DecayEffect for ExplosionRing {
    fn execute(&self, objects: &mut ObjectContainer, obj_index: usize) {
        let obj = &objects[obj_index].body;
        let r_k = self.r / 2. + 0.001;
        let r = r_k * f64::sin(PI / self.n as f64);
        let AbstractBody { x, y, vx, vy, .. } = *obj;
        let v = f64::sqrt(vx * vx + vy * vy);
        let m_i = obj.m_i / self.n as f64;
        let m_h = obj.m_h / self.n as f64;
        let color = obj.color.clone();
        objects.remove(obj_index);
        for i in 0..self.n {
            let delta = i as f64 * 2. * PI / self.n as f64;
            objects.add(Body {
                body: AbstractBody {
                    x: x + r_k * f64::cos(self.phi + delta),
                    y: y + r_k * f64::sin(self.phi + delta),
                    vx: vx + v * f64::cos(self.phi + delta),
                    vy: vy + v * f64::sin(self.phi + delta),
                    ax: 0.,
                    ay: 0.,
                    m_i,
                    m_h,
                    fixed: false,
                    color: color.clone(),
                    label: String::new(),
                    tau: self.tau,
                    substantial: self.substantial,
                    on_decay: if self.tau > 0. {
                        Some(Box::new(Destroy {}))
                    } else {
                        None
                    },
                    on_collision: None,
                },
                obj_type: ObjectType::Ball(Ball { r }),
            });
        }
    }
}

#[derive(Clone, Debug, Deserialize)]
struct Explosion {
    n: u32,
    tau: f64,
    r: f64,
    substantial: bool,
}
impl DecayEffect for Explosion {
    fn execute(&self, objects: &mut ObjectContainer, obj_index: usize) {
        let obj = &objects[obj_index].body;
        const SQRT_3_2: f64 = 0.8660254037844386; // 0.75_f64.sqrt();
        const V: [(f64, f64); 6] = [
            (1., 0.),
            (0.5, SQRT_3_2),
            (-0.5, SQRT_3_2),
            (-1., 0.),
            (-0.5, -SQRT_3_2),
            (0.5, -SQRT_3_2),
        ];
        /*
                const VX: [f64; 6] = [1., 0.5, -0.5, -1., -0.5, 0.5];
                const VY: [f64; 6] = [0., SQRT_3_2, SQRT_3_2, 0., -SQRT_3_2, -SQRT_3_2];
        */
        //  * *
        // * o *
        //  * *
        let mut rxr = 1.;
        let mut cf = (0., 0.);

        let mut crd: Vec<(f64, f64)> = Vec::new();
        // n = 5:
        // r^2=0 => (_n,_m) = (0,0)
        // r^2=1 => (_n,_m) = (0,1)
        // r^2=3 => (_n,_m) = (1,1)
        // r^2=4 => (_n,_m) = (0,2)
        // r^2=7 => (_n,_m) = (1,2)
        // ||_n*(1,0) + _m*(1, sqrt(3))/2||^2 <= n^2
        // (_n + _m/2)^2 + _m^2 3/4 = _n^2 + _n*_m + _m^2
        fn find_hex_coefs(rxr: &mut f64) -> (f64, f64) {
            loop {
                let _r = rxr.sqrt();
                for _m in ((SQRT_3_2 * _r) as u32)..=(_r as u32) {
                    for _n in 0..=_m {
                        if *rxr as u32 == _n * _n + _n * _m + _m * _m {
                            return (_m as f64, _n as f64);
                        }
                    }
                }
                *rxr += 1.;
            }
        }
        if self.n != 0 {
            crd.push((0., 0.));
        }
        for _ in 1..self.n {
            cf = find_hex_coefs(&mut rxr);
            let (mut vx_pref, mut vy_pref) = V[V.len() - 1];
            for (vx, vy) in &V {
                crd.push((vx_pref * cf.0 + vx * cf.1, vy_pref * cf.0 + vy * cf.1));
                if (cf.0 - cf.1).abs() > 0.1 && cf.1 != 0.0 {
                    crd.push((vx_pref * cf.1 + vx * cf.0, vy_pref * cf.1 + vy * cf.0));
                }
                vx_pref = *vx;
                vy_pref = *vy;
            }
        }
        let r = self.r / (2. * cf.0 + 1.) - 0.1;
        let m_i = obj.m_i / crd.len() as f64;
        let m_h = obj.m_h / crd.len() as f64;
        let v = f64::sqrt(obj.vx * obj.vx + obj.vy * obj.vy);
        let AbstractBody { x: x_0, y: y_0, .. } = *obj;
        let color = obj.color.clone();
        objects.remove(obj_index);
        for (xx, yy) in crd {
            objects.add(Body {
                body: AbstractBody {
                    x: x_0 + 2. * r * xx,
                    y: y_0 + 2. * r * yy,
                    vx: v * xx,
                    vy: v * yy,
                    ax: 0.,
                    ay: 0.,
                    m_i,
                    m_h,
                    fixed: false,
                    color: color.clone(),
                    label: String::new(),
                    tau: self.tau,
                    substantial: self.substantial,
                    on_decay: if self.tau > 0. {
                        Some(Box::new(Destroy {}))
                    } else {
                        None
                    },
                    on_collision: None,
                },
                obj_type: ObjectType::Ball(Ball { r }),
            });
        }
    }
}

#[derive(Clone, Deserialize)]
struct DisentangledCollisionEffect {
    #[serde(rename = "self")]
    self_effect: Option<Box<dyn DecayEffect>>,
    #[serde(rename = "other")]
    other_effect: Option<Box<dyn DecayEffect>>,
}
impl CollisionEffect for DisentangledCollisionEffect {
    fn execute(&self, objects: &mut ObjectContainer, obj_1: usize, obj_2: usize) {
        let ((e1, i1), (e2, i2)) = if obj_1 > obj_2 {
            ((&self.self_effect, obj_1), (&self.other_effect, obj_2))
        } else {
            ((&self.other_effect, obj_2), (&self.self_effect, obj_1))
        };
        if let Some(eff) = e1 {
            eff.execute(objects, i1);
        }
        if let Some(eff) = e2 {
            eff.execute(objects, i2);
        }
    }
    fn decompose(&self) -> [Option<Box<dyn DecayEffect>>; 2] {
        [self.self_effect.clone(), self.other_effect.clone()]
    }
    fn swapped(&self) -> Box<dyn CollisionEffect> {
        Box::new(DisentangledCollisionEffect {
            self_effect: self.other_effect.clone(),
            other_effect: self.self_effect.clone(),
        })
    }
}

#[derive(Clone, Deserialize)]
struct Merge {}
impl CollisionEffect for Merge {
    fn execute(&self, objects: &mut ObjectContainer, obj_1: usize, obj_2: usize) {
        let (o1, o2) = objects.get_pair(obj_1, obj_2);
        let (o1, o2) = (&mut o1.body, &mut o2.body);
        let m = o1.m_i + o2.m_i;
        o1.m_h += o2.m_h;
        o1.x = (o1.m_i * o1.x + o2.m_i * o2.x) / m;
        o1.y = (o1.m_i * o1.y + o1.m_i * o2.y) / m;
        o1.vx = (o1.m_i * o1.vx + o2.m_i * o2.vx) / m;
        o1.vy = (o1.m_i * o1.vy + o2.m_i * o2.vy) / m;
        o1.ax = (o1.m_i * o1.ax + o2.m_i * o2.ax) / m;
        o1.ay = (o1.m_i * o1.ay + o2.m_i * o2.ay) / m;
        o1.color.r = 0.5 * (o1.color.r + o2.color.r);
        o1.color.g = 0.5 * (o1.color.g + o2.color.g);
        o1.color.b = 0.5 * (o1.color.b + o2.color.b);
        o1.m_i = m;
        objects.remove(obj_2);
    }
    fn decompose(&self) -> [Option<Box<dyn DecayEffect>>; 2] {
        [None, None]
    }
    fn swapped(&self) -> Box<dyn CollisionEffect> {
        Box::new(self.clone())
    }
}

fn merge_collision_effects(
    eff_a: &dyn CollisionEffect,
    eff_b: &dyn CollisionEffect,
) -> Box<dyn CollisionEffect + 'static> {
    let [e_a1, e_b2] = eff_a.decompose();
    let [e_b1, e_a2] = eff_b.decompose();
    match (e_a2.or(e_a1), e_b2.or(e_b1)) {
        (None, None) => {}
        (a, b) => {
            return Box::new(DisentangledCollisionEffect {
                self_effect: a,
                other_effect: b,
            });
        }
    }
    eff_a.clone_box()
}
pub fn merge_optional_collision_effects(
    eff_a: &Option<Box<dyn CollisionEffect>>,
    eff_b: &Option<Box<dyn CollisionEffect>>,
) -> Option<Box<dyn CollisionEffect>> {
    match (eff_a, eff_b) {
        (Some(a), Some(b)) => Some(merge_collision_effects(a.as_ref(), b.as_ref())),
        (Some(a), None) => Some(a.clone()),
        (None, Some(b)) => Some(b.swapped()),
        (None, None) => None,
    }
}
impl Clone for Box<dyn CollisionEffect> {
    fn clone(&self) -> Box<dyn CollisionEffect> {
        self.clone_box()
    }
}
mod clone_inner {
    use super::{CollisionEffect, DecayEffect};
    pub trait CloneCollisionEffect {
        fn clone_box(&self) -> Box<dyn CollisionEffect>;
    }
    impl<T> CloneCollisionEffect for T
    where
        T: 'static + CollisionEffect + Clone,
    {
        fn clone_box(&self) -> Box<dyn CollisionEffect> {
            Box::new(self.clone())
        }
    }

    pub trait CloneDecayEffect {
        fn clone_box(&self) -> Box<dyn DecayEffect>;
    }

    impl<T> CloneDecayEffect for T
    where
        T: 'static + DecayEffect + Clone,
    {
        fn clone_box(&self) -> Box<dyn DecayEffect> {
            Box::new(self.clone())
        }
    }

    impl Clone for Box<dyn DecayEffect> {
        fn clone(&self) -> Box<dyn DecayEffect> {
            self.clone_box()
        }
    }
}
