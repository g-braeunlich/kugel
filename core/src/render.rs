use crate::graphics::{self, Canvas};
use crate::{kernel, objects};

/// Abstraction of rendering an object to a canvas
pub trait Render {
    fn render(self, canvas: &mut impl Canvas);
}

impl Render for &kernel::Polygon {
    fn render(self, canvas: &mut impl Canvas) {
        canvas.fill_polygon(
            &self.points,
            &graphics::Color {
                r: 1.,
                g: 1.,
                b: 1.,
                a: 1.,
            },
        );
    }
}

impl Render for &kernel::Kernel {
    fn render(self, canvas: &mut impl Canvas) {
        self.boundary.render(canvas);
        canvas.stroke_polygon(&self.boundary.points, &graphics::DEFAULT_LINE_STYLE);
        for o in &self.objects {
            o.render(canvas);
        }
    }
}

const LABEL_FONT: graphics::Font = graphics::Font {
    slant: graphics::FontSlant::Normal,
    weight: graphics::FontWeight::Normal,
    family: "Helvetica",
    size: 1.,
    color: graphics::Color::BLACK,
};

impl Render for &objects::Body {
    fn render(self, canvas: &mut impl Canvas) {
        WithAlpha {
            object: self,
            alpha: 1.,
        }
        .render(canvas);
    }
}

impl Render for WithAlpha<&objects::Body> {
    fn render(self, canvas: &mut impl Canvas) {
        let objects::Body {
            body: state,
            obj_type: type_props,
        } = &self.object;
        let alpha = self.alpha;
        match type_props {
            objects::ObjectType::Ball(ball) => WithAlpha {
                object: objects::BodyRef {
                    body: state,
                    obj_type: ball,
                },
                alpha,
            }
            .render(canvas),
            objects::ObjectType::Rectangle(rect) => WithAlpha {
                object: objects::BodyRef {
                    body: state,
                    obj_type: rect,
                },
                alpha,
            }
            .render(canvas),
        }
    }
}

impl Render for WithAlpha<objects::BodyRef<'_, objects::Ball>> {
    fn render(self, canvas: &mut impl Canvas) {
        let objects::BodyRef {
            body: state,
            obj_type: ball_props,
        } = self.object;
        if ball_props.r == 0. {
            canvas.draw_point(state.x, state.y, &state.color.with_alpha(self.alpha));
        } else {
            canvas.fill_circle(
                state.x,
                state.y,
                ball_props.r,
                &state.color.with_alpha(self.alpha),
            );
        }
        let mut font = LABEL_FONT;
        if !state.label.is_empty() {
            font.size = LABEL_FONT.size * ball_props.r;
            font.color = if state.color.get_lightness() <= 0.75 {
                graphics::Color::WHITE
            } else {
                graphics::Color::BLACK
            }
            .with_alpha(self.alpha);
            canvas.draw_text_centered(state.x, state.y, &state.label, &font);
        }
    }
}

impl Render for WithAlpha<objects::BodyRef<'_, objects::Rectangle>> {
    fn render(self, canvas: &mut impl Canvas) {
        let objects::BodyRef {
            body: state,
            obj_type: rect_props,
        } = self.object;
        let p = [
            (state.x - rect_props.w / 2., state.y - rect_props.h / 2.),
            (state.x + rect_props.w / 2., state.y - rect_props.h / 2.),
            (state.x + rect_props.w / 2., state.y + rect_props.h / 2.),
            (state.x - rect_props.w / 2., state.y + rect_props.h / 2.),
        ];
        canvas.fill_polygon(&p, &state.color.with_alpha(self.alpha));
    }
}

impl graphics::Color {
    fn with_alpha(&self, a: f64) -> Self {
        graphics::Color {
            a: self.a * a,
            ..*self
        }
    }
}

/// Helper struct to override the opacity of an object when rendering
pub struct WithAlpha<T> {
    pub object: T,
    pub alpha: f64,
}
