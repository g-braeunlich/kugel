#![allow(non_snake_case)]
#![allow(non_camel_case_types)]

use crate::effects::merge_optional_collision_effects;
use crate::graphics;
use crate::numerics;
use crate::objects::{AbstractBody, Ball, Body, BodyRef, BodyRefMut, ObjectType, Rectangle, Wall};
use serde::Deserialize;

const EPS: f64 = 1.0E-10;

pub struct Polygon {
    pub points: Vec<(f64, f64)>,
}

impl Polygon {
    fn new(w: f64, h: f64) -> Self {
        Polygon {
            points: vec![(0., 0.), (w, 0.), (w, h), (0., h)],
        }
    }
    fn empty() -> Self {
        Polygon { points: Vec::new() }
    }
    pub fn boundary_box(&self) -> Option<(f64, f64, f64, f64)> {
        let mut iter = self.points.iter();
        iter.next().map(|(x, y)| {
            iter.fold((*x, *x, *y, *y), |(x_min, x_max, y_min, y_max), (x, y)| {
                (
                    f64::min(x_min, *x),
                    f64::max(x_max, *x),
                    f64::min(y_min, *y),
                    f64::max(y_max, *y),
                )
            })
        })
    }
}

trait Movable {
    fn propagate(&mut self, dt: f64);
    fn accellerate(&mut self, dt: f64);
}

impl Movable for AbstractBody {
    fn propagate(&mut self, dt: f64) {
        self.x += dt * self.vx;
        self.y += dt * self.vy;
        if self.tau > 0. {
            self.tau -= dt;
        }
    }
    fn accellerate(&mut self, dt: f64) {
        self.vx += dt * self.ax;
        self.vy += dt * self.ay;
    }
}

trait Interaction {
    fn act(&self, objects: &mut ObjectContainer);
}

struct NullInteraction {}

impl Interaction for NullInteraction {
    fn act(&self, _objects: &mut ObjectContainer) {}
}

trait CentralForceBase {
    fn da(&self, dx: f64, dy: f64) -> f64;
    fn pair_interaction(&self, A: &mut Body, B: &mut Body) {
        let dx = A.x - B.x;
        let dy = A.y - B.y;
        let da = self.da(dx, dy);
        self.accellerate(A, dx, dy, -da * B.m_h);
        self.accellerate(B, dx, dy, da * A.m_h);
    }
    fn unilateral_interaction(&self, A: &Body, B: &mut Body) {
        let dx = A.x - B.x;
        let dy = A.y - B.y;
        let da = self.da(dx, dy);
        self.accellerate(B, dx, dy, da * A.m_h);
    }
    fn accellerate(&self, X: &mut Body, dx: f64, dy: f64, da: f64) {
        X.ax = da * dx;
        X.ay = da * dy;
    }
}

impl<T: CentralForceBase> Interaction for T {
    fn act(&self, objects: &mut ObjectContainer) {
        let (movable, fixed) = objects.split_at_fixed_mut();
        let n_movable = movable.len();
        for i in 0..n_movable - 1 {
            let (head, tail) = movable.split_at_mut(i + 1);
            let obj_1 = &mut head[i];
            for obj in tail {
                self.pair_interaction(obj_1, obj);
            }
        }
        for obj in movable {
            for actor in &mut *fixed {
                self.unilateral_interaction(actor, obj);
            }
        }
    }
}

struct CentralForceGeneric {
    pub G: f64,
    exp: f64,
}
fn r_exp_generic(exp: f64, dx: f64, dy: f64) -> f64 {
    f64::powf(dx * dx + dy * dy, 0.5 * (exp - 1.))
}
impl CentralForceBase for CentralForceGeneric {
    fn da(&self, dx: f64, dy: f64) -> f64 {
        self.G / r_exp_generic(self.exp, dx, dy)
    }
}

struct CentralForce__2 {
    pub G: f64,
}

fn r_exp__2(dx: f64, dy: f64) -> f64 {
    let rxr = dx * dx + dy * dy;
    rxr * rxr.sqrt()
}
impl CentralForceBase for CentralForce__2 {
    fn da(&self, dx: f64, dy: f64) -> f64 {
        self.G / r_exp__2(dx, dy)
    }
}

struct CentralForce__1 {
    pub G: f64,
}
fn r_exp__1(dx: f64, dy: f64) -> f64 {
    1. / (dx * dx + dy * dy)
}
impl CentralForceBase for CentralForce__1 {
    fn da(&self, dx: f64, dy: f64) -> f64 {
        self.G / r_exp__1(dx, dy)
    }
}

struct CentralForce_0 {
    pub G: f64,
}
fn r_exp_0(dx: f64, dy: f64) -> f64 {
    1. / f64::sqrt(dx * dx + dy * dy)
}
impl CentralForceBase for CentralForce_0 {
    fn da(&self, dx: f64, dy: f64) -> f64 {
        self.G / r_exp_0(dx, dy)
    }
}

struct CentralForce_1 {
    pub G: f64,
}
fn r_exp_1(dx: f64, dy: f64) -> f64 {
    0.5 / f64::ln(dx * dx + dy * dy)
}
impl CentralForceBase for CentralForce_1 {
    fn da(&self, dx: f64, dy: f64) -> f64 {
        self.G / r_exp_1(dx, dy)
    }
}

fn central_force_by_exponent(G: f64, exp: f64) -> Box<dyn Interaction> {
    if G == 0. {
        return Box::new(NullInteraction {});
    }
    if (exp.floor() - exp).abs() < f64::EPSILON {
        match exp as i32 {
            0 => {
                return Box::new(CentralForce_0 { G });
            }
            -1 => {
                return Box::new(CentralForce__1 { G });
            }
            -2 => {
                return Box::new(CentralForce__2 { G });
            }
            _ => {}
        }
    }
    Box::new(CentralForceGeneric { G, exp })
}

fn point_collision(X: &mut AbstractBody, Y: &mut AbstractBody, dx: f64, dy: f64) {
    let (mu_Y, mu_X) = mu_movable(X, Y);
    let k = (dx * (X.vx - Y.vx) + dy * (X.vy - Y.vy)) / (dx * dx + dy * dy);
    X.vx -= k * mu_X * dx;
    X.vy -= k * mu_X * dy;
    Y.vx += k * mu_Y * dx;
    Y.vy += k * mu_Y * dy;
}
fn mu_movable(X: &AbstractBody, Y: &AbstractBody) -> (f64, f64) {
    match (
        X.m_i.is_infinite() || X.fixed,
        Y.m_i.is_infinite() || Y.fixed,
    ) {
        (false, false) => {
            // m_X, m_Y < inf
            let k = 2. / (X.m_i + Y.m_i);
            (X.m_i * k, Y.m_i * k)
        }
        (false, true) => (0., 2.), // m_Y == inf
        (true, false) => (2., 0.), // m_X == inf
        (true, true) => (1., 1.),  // m_X, m_Y == inf
    }
}

trait Event {
    fn execute(&self, objects: &mut ObjectContainer);
}
struct DecayEvent {
    obj_index: usize,
}
impl Event for DecayEvent {
    fn execute(&self, objects: &mut ObjectContainer) {
        let on_decay = objects[self.obj_index].on_decay.as_ref().cloned();
        if let Some(ev) = on_decay {
            ev.execute(objects, self.obj_index)
        }
    }
}
trait CollisionPredictable<T> {
    fn t_occurence(self, other: T) -> f64;
}
trait Collidable<T> {
    fn collide(self, other: T);
}

impl CollisionPredictable<&Body> for &Body {
    fn t_occurence(self, other: &Body) -> f64 {
        match (&self.obj_type, &other.obj_type) {
            (ObjectType::Ball(t1), ObjectType::Ball(t2)) => {
                self.downcast_ref(t1).t_occurence(other.downcast_ref(t2))
            }
            (ObjectType::Ball(t1), ObjectType::Rectangle(t2)) => {
                self.downcast_ref(t1).t_occurence(other.downcast_ref(t2))
            }
            (ObjectType::Rectangle(t1), ObjectType::Ball(t2)) => {
                self.downcast_ref(t2).t_occurence(other.downcast_ref(t1))
            }
            (ObjectType::Rectangle(t1), ObjectType::Rectangle(t2)) => {
                self.downcast_ref(t1).t_occurence(other.downcast_ref(t2))
            }
        }
    }
}

impl<'a> CollisionPredictable<BodyRef<'a, Ball>> for BodyRef<'a, Ball> {
    /// Time until collision of ball A with ball B, or -1, if they
    /// do not collide
    fn t_occurence(self, other: BodyRef<'a, Ball>) -> f64 {
        let R = if !self.body.substantial || !other.body.substantial {
            f64::max(self.obj_type.r, other.obj_type.r)
        } else {
            self.obj_type.r + other.obj_type.r
        };
        t_occurence_points(
            other.body.x - self.body.x,
            other.body.y - self.body.y,
            other.body.vx - self.body.vx,
            other.body.vy - self.body.vy,
            other.body.ax - self.body.ax,
            other.body.ay - self.body.ay,
            R,
        )
    }
}
impl<'a> Collidable<BodyRefMut<'a, Ball>> for BodyRefMut<'a, Ball> {
    fn collide(self, other: BodyRefMut<'a, Ball>) {
        let dx = self.body.x - other.body.x;
        let dy = self.body.y - other.body.y;
        point_collision(self.body, other.body, dx, dy);
    }
}
impl<'a> CollisionPredictable<BodyRef<'a, Rectangle>> for BodyRef<'a, Rectangle> {
    /// Time until collision of rectangle A with rectangle B, or -1, if they do not
    /// do not collide
    fn t_occurence(self, other: BodyRef<'a, Rectangle>) -> f64 {
        let (mut w, mut h) = if !self.body.substantial || !other.body.substantial {
            (
                f64::max(self.obj_type.w, other.obj_type.w),
                f64::max(self.obj_type.h, other.obj_type.h),
            )
        } else {
            (
                self.obj_type.w + other.obj_type.w,
                other.obj_type.h + other.obj_type.h,
            )
        };
        // max( dx(t)/w, dy(t)/h ) = 1
        w *= 0.5;
        h *= 0.5;
        t_occurence_rect(
            [other.body.x - self.body.x, other.body.y - self.body.y],
            [other.body.vx - self.body.vx, other.body.vy - self.body.vy],
            [other.body.ax - self.body.ax, other.body.ay - self.body.ay],
            0.5 * w,
            0.5 * h,
            0.,
        )
    }
}
impl<'a> Collidable<BodyRefMut<'a, Rectangle>> for BodyRefMut<'a, Rectangle> {
    fn collide(self, other: BodyRefMut<'a, Rectangle>) {
        /* ______
         * |\  i/|
         * | \ /~i
         * |  x  |
         * | / \j|
         * |/ ~j\|
         * -------
         */
        const NX: [[f64; 2]; 2] = [[0., 1.], [-1., 0.]];
        const NY: [[f64; 2]; 2] = [[-1., 0.], [0., 1.]];
        let mut dx = self.body.x - other.body.x;
        let mut dy = self.body.y - other.body.y;
        let (mu_X, mu_Y) = mu_movable(self.body, other.body);
        let i = (self.obj_type.w * dy - other.obj_type.h * dx > 0.) as usize;
        let j = (self.obj_type.w * dy + other.obj_type.h * dx > 0.) as usize;
        dx = NX[i][j];
        dy = NY[i][j];
        let k = dx * (self.body.vx - other.body.vx) + dy * (self.body.vy - other.body.vy);
        self.body.vx += k * mu_X * dx;
        self.body.vy += k * mu_X * dy;
        other.body.vx -= k * mu_Y * dx;
        other.body.vy -= k * mu_Y * dy;
    }
}
impl<'a> CollisionPredictable<BodyRef<'a, Rectangle>> for BodyRef<'a, Ball> {
    /// Time until collision of Ball A with rectangle B, or -1, if they do not
    /// do not collide
    fn t_occurence(self, other: BodyRef<'a, Rectangle>) -> f64 {
        let (w, h) = if !self.body.substantial || !other.body.substantial {
            (
                f64::max(self.obj_type.r, other.obj_type.w),
                f64::max(self.obj_type.r, other.obj_type.h),
            )
        } else {
            (
                self.obj_type.r + 0.5 * other.obj_type.w,
                self.obj_type.r + 0.5 * other.obj_type.h,
            )
        };
        let dx = other.body.x - self.body.x;
        let dy = other.body.y - self.body.y;
        let dvx = other.body.vx - self.body.vx;
        let dvy = other.body.vy - self.body.vy;
        let dax = other.body.ax - self.body.ax;
        let day = other.body.ay - self.body.ay;
        let mut t = t_occurence_rect([dx, dy], [dvx, dvy], [dax, day], w, h, self.obj_type.r);
        if t >= 0. {
            return t;
        }
        let mut _t;
        for sx in &[-0.5, 0.5] {
            for sy in &[-0.5, 0.5] {
                _t = t_occurence_points(
                    dx + sx * other.obj_type.w,
                    dy + sy * other.obj_type.h,
                    dvx,
                    dvy,
                    dax,
                    day,
                    self.obj_type.r,
                );
                if _t >= 0. && (t < 0. || _t < t) {
                    t = _t;
                }
            }
        }
        t
    }
}
impl<'a> Collidable<BodyRefMut<'a, Rectangle>> for BodyRefMut<'a, Ball> {
    fn collide(self, other: BodyRefMut<'a, Rectangle>) {
        let X = self.body;
        let Y = other.body;
        let dx = X.x - Y.x;
        let dy = X.y - Y.y;
        if f64::abs(dy) <= 0.5 * other.obj_type.h {
            let (mu_A, mu_B) = mu_movable(X, Y);
            let Vx2 = X.vx * mu_A + Y.vx * mu_B;
            X.vx = Vx2 - X.vx;
            Y.vx = Vx2 - Y.vx;
            return;
        }
        if f64::abs(dx) <= 0.5 * other.obj_type.w {
            let (mu_A, mu_B) = mu_movable(X, Y);
            let Vx2 = X.vy * mu_A + Y.vy * mu_B;
            X.vy = Vx2 - X.vy;
            Y.vy = Vx2 - Y.vy;
            return;
        }
        point_collision(
            X,
            Y,
            dx + 0.5 * other.obj_type.w.copysign(dx),
            dy + 0.5 * other.obj_type.h.copysign(dy),
        );
    }
}
impl CollisionPredictable<&Body> for &Wall {
    fn t_occurence(self, other: &Body) -> f64 {
        match &other.obj_type {
            ObjectType::Ball(t) => self.t_occurence(other.downcast_ref(t)),
            ObjectType::Rectangle(t) => self.t_occurence(other.downcast_ref(t)),
        }
    }
}
impl<'a> CollisionPredictable<BodyRef<'a, Ball>> for &Wall {
    /// Time until collision of ball X with wall self.wall, or -1, if they
    /// do not collide
    fn t_occurence(self, other: BodyRef<'a, Ball>) -> f64 {
        let BodyRef { body, obj_type } = other;
        let v_n = body.vx * self.nx + body.vy * self.ny;
        let x_n = body.x * self.nx + body.y * self.ny;
        let a_n = body.ax * self.nx + body.ay * self.ny;
        let delta = self.d + obj_type.r - x_n;
        t_occurence_wall(delta, v_n, a_n)
    }
}
impl<'a> Collidable<BodyRefMut<'a, Ball>> for &'a Wall {
    fn collide(self, other: BodyRefMut<'a, Ball>) {
        wall_collision(self, other.body);
    }
}
impl<'a> CollisionPredictable<BodyRef<'a, Rectangle>> for &Wall {
    /// Time until collision of rectangle X with wall self.wall, or -1, if they
    /// do not collide
    fn t_occurence(self, other: BodyRef<'a, Rectangle>) -> f64 {
        let BodyRef { body, obj_type } = other;
        let v_n = body.vx * self.nx + body.vy * self.ny;
        let x_n = body.x * self.nx + body.y * self.ny;
        let a_n = body.ax * self.nx + body.ay * self.ny;
        let r =
            0.5 * (self.nx * obj_type.w.copysign(self.nx) + self.ny * obj_type.h.copysign(self.ny));
        let delta = self.d + r - x_n;
        t_occurence_wall(delta, v_n, a_n)
    }
}
impl<'a> Collidable<BodyRefMut<'a, Rectangle>> for &'a Wall {
    fn collide(self, other: BodyRefMut<'a, Rectangle>) {
        wall_collision(self, other.body);
    }
}
#[derive(Debug)]
struct BodyCollision {
    i: usize,
    j: usize,
}
fn elastic_collision(o1: &mut Body, o2: &mut Body) {
    let (b1, b2) = (&mut o1.body, &mut o2.body);
    match (&o1.obj_type, &o2.obj_type) {
        (ObjectType::Ball(t1), ObjectType::Ball(t2)) => {
            b1.downcast_ref_mut(t1).collide(b2.downcast_ref_mut(t2))
        }

        (ObjectType::Ball(t1), ObjectType::Rectangle(t2)) => {
            b1.downcast_ref_mut(t1).collide(b2.downcast_ref_mut(t2))
        }

        (ObjectType::Rectangle(t1), ObjectType::Ball(t2)) => {
            b2.downcast_ref_mut(t2).collide(b1.downcast_ref_mut(t1))
        }

        (ObjectType::Rectangle(t1), ObjectType::Rectangle(t2)) => {
            b1.downcast_ref_mut(t1).collide(b2.downcast_ref_mut(t2))
        }
    }
}
impl Event for BodyCollision {
    fn execute(&self, objects: &mut ObjectContainer) {
        let (o1, o2) = objects.get_pair(self.i, self.j);
        if let Some(eff) = merge_optional_collision_effects(&o1.on_collision, &o2.on_collision) {
            eff.execute(objects, self.i, self.j)
        } else {
            elastic_collision(o1, o2)
        }
    }
}

fn t_occurence_rect(d: [f64; 2], dv: [f64; 2], da: [f64; 2], w: f64, h: f64, r: f64) -> f64 {
    let mut t = t_occurence_wall(w - d[0], dv[0], da[0]);
    if f64::abs(d[1] + t * (dv[1] + 0.5 * t * da[1])) > h - r {
        t = -1.;
    }
    let mut _t = t_occurence_wall(w + d[0], -dv[0], -da[0]);
    if _t >= 0. && f64::abs(d[1] + _t * (dv[1] + 0.5 * _t * da[1])) <= h - r && (t < 0. || _t < t) {
        t = _t;
    }
    _t = t_occurence_wall(h - d[1], dv[1], da[1]);
    if _t >= 0. && (f64::abs(d[0] + _t * (dv[0] + 0.5 * _t * da[0])) <= w - r) && (t < 0. || _t < t)
    {
        t = _t;
    }
    _t = t_occurence_wall(h + d[1], -dv[1], -da[1]);
    if _t >= 0. && f64::abs(d[0] + _t * (dv[0] + 0.5 * _t * da[0])) <= w - r && (t < 0. || _t < t) {
        t = _t;
    }
    t
}

struct WallCollision<'a> {
    wall: &'a Wall,
    i: usize,
}
impl<'a> Event for WallCollision<'a> {
    fn execute(&self, objects: &mut ObjectContainer) {
        let obj = &mut objects[self.i];
        match &obj.obj_type {
            ObjectType::Ball(t) => self.wall.collide(BodyRefMut {
                body: &mut obj.body,
                obj_type: t,
            }),
            ObjectType::Rectangle(t) => self.wall.collide(BodyRefMut {
                body: &mut obj.body,
                obj_type: t,
            }),
        }
    }
}

/// Time until point A reaches B in distance R, or -1, if
/// they do not meet
/// \| dr + t dv \|^2 = R^2  <=>  t^2 dv^2 + t 2 <dr, dv> + dr^2 - R^2 = 0
/// => t = -<dr, dv>/dv^2 +- \sqrt{<dr, dv>^2 - dv^2 (dr^2-R^2)}/dv^2 = (dr^2-R^2)/(-<dr, dv> -+ \sqrt{<dr, dv>^2 - dv^2 (dr^2-R^2)})
fn t_occurence_points_no_accel(dx: f64, dy: f64, dvx: f64, dvy: f64, R: f64) -> f64 {
    let S = -dx * dvx - dy * dvy; // - dr * dv
    if S <= 0. {
        return -1.;
    } // Distance increases between the points.
    let dvQ = dvx * dvx + dvy * dvy; // dv^2
                                     // b = |dx*dvy - dy*dvx|/|dv| is impact parameter
    let D = -(dx * dvy - dy * dvx) * (dx * dvy - dy * dvx) + R * R * dvQ;
    if D < 0. {
        return -1.;
    } // b > R => balls do not touch
    (dx * dx + dy * dy - R * R) / (S + D.sqrt())
    // Solution t = (S+sqrt(D))/dvQ would be after a move through
}
fn t_occurence_points(dx: f64, dy: f64, dvx: f64, dvy: f64, dax: f64, day: f64, R: f64) -> f64 {
    // \| dx + t dv + 1/2 t^2 da \|^2 = R^2
    // <=> 0 = t^4/4 \|da\|^2 + t^3 <da, dv> + (<da, dx> + \|dv\|^2) t^2 + 2 <dx, dv> t + \|dx\|^2 - R^2
    if f64::abs(dax) < EPS && f64::abs(day) < EPS {
        return t_occurence_points_no_accel(dx, dy, dvx, dvy, R);
    }
    let p = [
        dx * dx + dy * dy - R * R,
        2. * (dx * dvx + dy * dvy),
        (dax * dx + day * dy) + (dvx * dvx + dvy * dvy),
        dax * dvx + day * dvy,
        0.25 * (dax * dax + day * day),
    ];
    if f64::abs(p[0]) < EPS && p[1] <= 0. {
        return 0.;
    }
    numerics::zero_p4(&p, EPS)
}

fn t_occurence_wall(delta: f64, v_n: f64, a_n: f64) -> f64 {
    // <x + vt + 1/2 a t^2, n> - (d+r) = 0
    // delta := r + d - x_n
    // => t = -(v_n +- sqrt(v_n^2 + 2 a_n delta))/a_n
    //      = 2 delta/(v_n -+ sqrt(v_n^2 + 2 a_n delta))
    // a_n t + v_n < 0
    // => s sqrt(v_n^2 + 2 a_n delta) > 0
    // s = 1
    // => t = -(v_n + sqrt(v_n^2 + 2 a_n delta))/a_n
    //      = 2 delta/(v_n - sqrt(v_n^2 + 2 a_n delta))
    let D = v_n * v_n + 2. * a_n * delta;
    if D < 0. || (v_n == 0. && a_n == 0.) {
        return -1.;
    }
    let q = v_n + D.sqrt().copysign(v_n);
    if v_n < 0. {
        return 2. * delta / q;
    }
    -q / a_n
}
fn wall_collision(w: &Wall, obj: &mut AbstractBody) {
    let k = 2. * (w.nx * obj.vx + w.ny * obj.vy);
    obj.vx -= k * w.nx;
    obj.vy -= k * w.ny;
}

#[derive(Deserialize)]
#[serde(from = "SerializedKernel")]
pub struct Kernel {
    pub objects: ObjectContainer,
    pub walls: Vec<Wall>,
    pub boundary: Polygon,
    pub mu: f64,
    pub g_x: f64,
    pub g_y: f64,
    interaction: Box<dyn Interaction>,
}

impl Kernel {
    /// Assumption: fixed objects are at the end of self.objects
    fn next_event<'a>(
        objects: &[Body],
        walls: &'a [Wall],
        T: f64,
    ) -> (f64, Option<Box<dyn Event + 'a>>) {
        let mut dt = T;
        let mut occurring_event: Option<Box<dyn Event>> = None;
        for i in 0..objects.len() {
            let o1 = &objects[i];
            if o1.tau < dt && o1.on_decay.is_some() {
                occurring_event = Some(Box::new(DecayEvent { obj_index: i }));
            }
            if o1.fixed {
                break;
            }
            for (j, o2) in objects.iter().enumerate().skip(i + 1) {
                let t_s = o1.t_occurence(o2);
                if -EPS <= t_s && t_s < dt {
                    occurring_event = Some(Box::new(BodyCollision { i, j }));
                    dt = t_s;
                }
            }
            for wall in walls {
                let t_s = wall.t_occurence(o1);
                if -EPS <= t_s && t_s < dt {
                    occurring_event = Some(Box::new(WallCollision { i, wall }));
                    dt = t_s;
                }
            }
        }
        (dt, occurring_event)
    }
    pub fn step(&mut self, delta_t: f64) {
        let mut T = delta_t;
        while T > 0. {
            let (dt, occurring_event) = Self::next_event(&self.objects, &self.walls, T);
            T -= dt;

            // Leapfrog method - kick-drift-kick form:
            for o in &mut self.objects {
                // kick:
                o.accellerate(0.5 * dt);
                // drift:
                o.propagate(dt);
            }
            if let Some(evt) = occurring_event {
                evt.execute(&mut self.objects);
            }
            // kick:
            self.interaction.act(&mut self.objects);
            for o in &mut self.objects {
                o.accellerate(0.5 * dt);
            }
        }

        for o in self.objects.iter_mut() {
            Self::friction(self.mu, o);
            Self::uniform_field_force(self.g_x, self.g_y, o);
        }
    }
    fn uniform_field_force(g_x: f64, g_y: f64, obj: &mut Body) {
        obj.vx += g_x;
        obj.vy += g_y;
    }

    fn friction(mu: f64, obj: &mut Body) {
        if mu == 0. {
            return;
        }
        let vxv = (obj.vx) * (obj.vx) + (obj.vy) * (obj.vy);
        if vxv < mu * mu {
            obj.vx = 0.;
            obj.vy = 0.;
        } else if vxv != 0. {
            let v = vxv.sqrt();
            obj.vx *= (v - mu) / v;
            obj.vy *= (v - mu) / v;
        }
    }
    fn set_boundary(&mut self, polygon: Polygon) {
        self.boundary = polygon;
        self.walls = (&self.boundary).into();
    }
    pub fn set_central_force(&mut self, G: f64, exp: f64) {
        self.interaction = central_force_by_exponent(G, exp);
    }
    fn default_configuration(&mut self, w: f64, h: f64, n: usize) {
        const R: f64 = 10.;
        const V_MAX: f64 = 1.;
        const M_MAX: f64 = 1000.; // maximal mass for objects
        const M_MIN: f64 = 100.; // minimal mass for objects
        for _ in 0..n {
            self.objects.add(Body {
                body: AbstractBody {
                    x: R + rand::random::<f64>() * (w - 2. * R),
                    y: R + rand::random::<f64>() * (h - 2. * R),
                    vx: (2. * rand::random::<f64>() - 1.) * V_MAX,
                    vy: (2. * rand::random::<f64>() - 1.) * V_MAX,
                    ax: 0.,
                    ay: 0.,
                    m_i: M_MIN + rand::random::<f64>() * (M_MAX - M_MIN),
                    m_h: 0.,
                    fixed: false,
                    substantial: true,
                    tau: 0.,
                    color: graphics::Color {
                        r: rand::random(),
                        g: rand::random(),
                        b: rand::random(),
                        a: 1.,
                    },
                    label: String::from(""),
                    on_collision: None,
                    on_decay: None,
                },
                obj_type: ObjectType::Ball(Ball { r: R }),
            });
        }
        self.set_boundary(Polygon::new(w, h));
    }
    pub fn new(w: f64, h: f64, n: usize) -> Self {
        let mut core = Kernel {
            objects: ObjectContainer::new(),
            walls: Vec::new(),
            boundary: Polygon::empty(),
            mu: 0.,
            g_x: 0.,
            g_y: 0.,
            interaction: Box::new(NullInteraction {}),
        };
        core.default_configuration(w, h, n);
        core
    }
}

#[derive(Default)]
pub struct ObjectContainer {
    all: Vec<Body>,
    n_movable: usize,
}

impl ObjectContainer {
    fn new() -> Self {
        ObjectContainer {
            ..Default::default()
        }
    }
    pub fn get_pair(&'_ mut self, i: usize, j: usize) -> (&'_ mut Body, &'_ mut Body) {
        self.all.get_pair(i, j)
    }
    pub fn movables_mut(&mut self) -> &mut [Body] {
        &mut self.all[0..self.n_movable]
    }
    pub fn split_at_fixed_mut(&mut self) -> (&mut [Body], &mut [Body]) {
        let n = self.n_movable;
        self.split_at_mut(n)
    }
    pub fn add(&mut self, obj: Body) {
        if obj.fixed {
            self.all.push(obj);
        } else {
            self.all.insert(self.n_movable, obj);
            self.n_movable += 1;
        }
    }
    pub fn remove(&mut self, index: usize) {
        let movable = !self.all[index].fixed;
        self.all.remove(index);
        if movable {
            self.n_movable -= 1;
        }
    }
}
impl std::ops::Deref for ObjectContainer {
    type Target = [Body];
    fn deref(&self) -> &Self::Target {
        self.all.deref()
    }
}
impl std::ops::DerefMut for ObjectContainer {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.all.deref_mut()
    }
}

impl<'a> IntoIterator for &'a mut ObjectContainer {
    type Item = &'a mut Body;
    type IntoIter = <&'a mut Vec<Body> as IntoIterator>::IntoIter;
    fn into_iter(self) -> Self::IntoIter {
        self.all.iter_mut()
    }
}
impl<'a> IntoIterator for &'a ObjectContainer {
    type Item = &'a Body;
    type IntoIter = <&'a Vec<Body> as IntoIterator>::IntoIter;
    fn into_iter(self) -> Self::IntoIter {
        self.all.iter()
    }
}

impl From<Vec<Body>> for ObjectContainer {
    fn from(other: Vec<Body>) -> Self {
        let (mut fixed, mut all): (Vec<Body>, Vec<Body>) = other.into_iter().partition(|o| o.fixed);
        let n_movable = all.len();
        all.append(&mut fixed);
        ObjectContainer { all, n_movable }
    }
}

pub trait GetMutPair<'a, T> {
    fn get_pair(self, i: usize, j: usize) -> (&'a mut T, &'a mut T);
}
impl<'a, T> GetMutPair<'a, T> for &'a mut [T] {
    /// panics if j <= i
    fn get_pair(self, i: usize, j: usize) -> (&'a mut T, &'a mut T) {
        let (a, b) = self.split_at_mut(i + 1);
        (&mut a[i], &mut b[j - i - 1])
    }
}

impl From<&Polygon> for Vec<Wall> {
    fn from(val: &Polygon) -> Self {
        if val.points.len() < 2 {
            return Vec::new();
        }
        let mut walls = Vec::new();
        for pair in val.points.windows(2) {
            walls.push(Wall::new(pair[0].0, pair[0].1, pair[1].0, pair[1].1));
        }
        walls.push(Wall::new(
            val.points.last().unwrap().0,
            val.points.last().unwrap().1,
            val.points[0].0,
            val.points[0].1,
        ));
        walls
    }
}

#[derive(Deserialize)]
struct SerializedKernel {
    pub movables: Vec<Body>,
    pub boundary_polygon: Vec<SerializedPoint>,
    #[serde(default)]
    pub mu: f64,
    #[serde(default)]
    pub g_x: f64,
    #[serde(default)]
    pub g_y: f64,
    #[serde(default)]
    pub G: f64,
    #[serde(default = "default_g_exp")]
    pub g_exp: f64,
}

fn default_g_exp() -> f64 {
    2.0
}

#[derive(Deserialize)]
struct SerializedPoint {
    x: f64,
    y: f64,
}
impl From<SerializedKernel> for Kernel {
    fn from(other: SerializedKernel) -> Self {
        let boundary = Polygon {
            points: other
                .boundary_polygon
                .into_iter()
                .map(|SerializedPoint { x, y }| (x, y))
                .collect(),
        };
        Kernel {
            objects: ObjectContainer::from(other.movables),
            walls: (&boundary).into(),
            boundary,
            mu: other.mu,
            g_x: other.g_x,
            g_y: other.g_y,
            interaction: central_force_by_exponent(other.G, other.g_exp),
        }
    }
}
