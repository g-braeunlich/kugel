use crate::effects::{CollisionEffect, DecayEffect};
use crate::graphics;
use serde::Deserialize;

/// Static properties of a ball
#[derive(Clone, Debug, Deserialize)]
pub struct Ball {
    /// Radius
    pub r: f64,
}
#[derive(Clone, Debug, Deserialize)]
pub struct Wall {
    /// x components of normal
    pub nx: f64,
    /// y components of normal
    pub ny: f64,
    /// Distance of wall to origin
    pub d: f64,
}
/// Static properties of a rectangle
#[derive(Clone, Debug, Deserialize)]
pub struct Rectangle {
    /// Width
    pub w: f64,
    /// Height
    pub h: f64,
}

impl Wall {
    pub fn new(x1: f64, y1: f64, x2: f64, y2: f64) -> Self {
        let mut nx = -(y2 - y1);
        let mut ny = x2 - x1;
        let dn = f64::hypot(nx, ny);
        if dn == 0. {
            nx = 1.;
            ny = 0.;
        } else {
            nx /= dn;
            ny /= dn;
        }
        Self {
            nx,
            ny,
            d: x1 * nx + y1 * ny,
        }
    }
}

#[derive(Clone, Debug, Deserialize)]
#[serde(tag = "__type__")]
pub enum ObjectType {
    Ball(Ball),
    Rectangle(Rectangle),
}

#[derive(Default, Clone, Deserialize)]
pub struct AbstractBody {
    /// Position (x)
    pub x: f64,
    /// Position (y)
    pub y: f64,
    /// Velocity (x)
    pub vx: f64,
    /// Velocity (y)
    pub vy: f64,
    /// Accelleration (x)
    #[serde(default)]
    pub ax: f64,
    /// Accelleration (y)
    #[serde(default)]
    pub ay: f64,
    /// Inert mass
    pub m_i: f64,
    /// Heavy mass
    #[serde(default)]
    pub m_h: f64,
    /// Durability
    #[serde(default)]
    pub tau: f64,
    pub substantial: bool,
    pub fixed: bool,
    pub color: graphics::Color,
    #[serde(default)]
    pub label: String,
    #[serde(default)]
    pub on_decay: Option<Box<dyn DecayEffect>>,
    #[serde(default)]
    pub on_collision: Option<Box<dyn CollisionEffect>>,
}

#[derive(Clone, Deserialize)]
pub struct Body {
    #[serde(flatten)]
    pub body: AbstractBody,
    #[serde(flatten)]
    pub obj_type: ObjectType,
}

pub struct BodyRef<'a, T> {
    pub body: &'a AbstractBody,
    pub obj_type: &'a T,
}
pub struct BodyRefMut<'a, T> {
    pub body: &'a mut AbstractBody,
    pub obj_type: &'a T,
}
impl std::ops::Deref for Body {
    type Target = AbstractBody;
    fn deref(&self) -> &Self::Target {
        &self.body
    }
}
impl std::ops::DerefMut for Body {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.body
    }
}

impl AbstractBody {
    pub fn downcast_ref<'a, T>(&'a self, t: &'a T) -> BodyRef<'a, T> {
        BodyRef {
            body: self,
            obj_type: t,
        }
    }
    pub fn downcast_ref_mut<'a, T>(&'a mut self, t: &'a T) -> BodyRefMut<'a, T> {
        BodyRefMut {
            body: self,
            obj_type: t,
        }
    }
}
