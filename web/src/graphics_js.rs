//! js graphics backend

use kugel_core::graphics;
use wasm_bindgen::JsValue;
use web_sys::CanvasRenderingContext2d;

/// js canvas implementing [graphics::Canvas]
pub struct Canvas(pub CanvasRenderingContext2d);

fn into_js_color(color: &graphics::Color) -> JsValue {
    JsValue::from_str(&format!(
        "rgba({}, {}, {}, {})",
        (color.r * 255.) as u8,
        (color.g * 255.) as u8,
        (color.b * 255.) as u8,
        (color.a * 255.) as u8
    ))
}

fn polygon_path<'a>(ctx: &CanvasRenderingContext2d, p: impl IntoIterator<Item = &'a (f64, f64)>) {
    ctx.begin_path();
    let mut iter = p.into_iter();
    if let Some((x, y)) = iter.next() {
        ctx.move_to(*x, *y);
    } else {
        return;
    }
    for (x, y) in iter {
        ctx.line_to(*x, *y);
    }
}

fn circle_path(ctx: &CanvasRenderingContext2d, x: f64, y: f64, r: f64) {
    ctx.begin_path();
    ctx.arc(x, y, r, 0., 2. * std::f64::consts::PI).unwrap();
}

fn set_line_style(canvas: &mut CanvasRenderingContext2d, line_style: &graphics::LineStyle) {
    canvas.set_line_width(line_style.width);
    canvas.set_stroke_style(&into_js_color(&line_style.color));
}

impl graphics::StrokeRectangle for Canvas {
    fn stroke_rectangle(
        &mut self,
        x: f64,
        y: f64,
        w: f64,
        h: f64,
        line_style: &graphics::LineStyle,
    ) {
        set_line_style(&mut self.0, line_style);
        self.0.stroke_rect(x, y, w, h);
    }
}
impl graphics::FillRectangle for Canvas {
    fn fill_rectangle(&mut self, x: f64, y: f64, w: f64, h: f64, color: &graphics::Color) {
        self.0.set_fill_style(&into_js_color(color));
        self.0.fill_rect(x, y, w, h);
    }
}
impl graphics::StrokePolygon for Canvas {
    fn stroke_polygon(&mut self, p: &[(f64, f64)], line_style: &graphics::LineStyle) {
        set_line_style(&mut self.0, line_style);
        polygon_path(&self.0, p);
        self.0.close_path();
        self.0.stroke();
    }
}
impl graphics::StrokePath for Canvas {
    fn stroke_path<'a>(
        &mut self,
        p: impl IntoIterator<Item = &'a (f64, f64)>,
        line_style: &graphics::LineStyle,
    ) {
        set_line_style(&mut self.0, line_style);
        polygon_path(&self.0, p);
        self.0.stroke();
    }
}
impl graphics::FillPolygon for Canvas {
    fn fill_polygon(&mut self, p: &[(f64, f64)], color: &graphics::Color) {
        self.0.set_fill_style(&into_js_color(color));
        polygon_path(&self.0, p);
        self.0.close_path();
        self.0.fill();
    }
}
impl graphics::FillCircle for Canvas {
    fn fill_circle(&mut self, x: f64, y: f64, r: f64, color: &graphics::Color) {
        self.0.begin_path();
        self.0.set_fill_style(&into_js_color(color));
        circle_path(&self.0, x, y, r);
        self.0.fill();
    }
}
impl graphics::StrokeCircle for Canvas {
    fn stroke_circle(&mut self, x: f64, y: f64, r: f64, line_style: &graphics::LineStyle) {
        set_line_style(&mut self.0, line_style);
        circle_path(&self.0, x, y, r);
        self.0.stroke();
    }
}
impl graphics::DrawPoint for Canvas {
    fn draw_point(&mut self, x: f64, y: f64, color: &graphics::Color) {
        self.0.set_fill_style(&into_js_color(color));
        use graphics::FillRectangle;
        self.fill_rectangle(x, y, 1., 1., color)
    }
}
impl graphics::DrawTextCentered for Canvas {
    fn draw_text_centered(&mut self, x: f64, y: f64, text: &str, font: &graphics::Font) {
        let font_weight = match font.weight {
            graphics::FontWeight::Bold => "bold",
            graphics::FontWeight::Normal => "normal",
        };
        self.0
            .set_font(&format!("{} {}px {}", font_weight, font.size, font.family));
        self.0.set_fill_style(&into_js_color(&font.color));
        self.0.stroke_text(text, x, y).unwrap();
    }
}
