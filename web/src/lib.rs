mod graphics_js;

use kugel_core::{kernel::Kernel, render::Render};
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

/// Entry point
/// Automatically invoked after the wasm module is instantiated.
#[wasm_bindgen(start)]
async fn init() -> Result<(), JsValue> {
    let doc = document();
    let canvas_elements = doc.get_elements_by_tag_name("canvas");
    let player = std::rc::Rc::new(std::cell::RefCell::new(Player::new()));
    for i in 0..canvas_elements.length() {
        let Some(canvas) = canvas_elements.item(i) else {
            continue;
        };
        let canvas = canvas.dyn_into::<web_sys::HtmlCanvasElement>()?;
        let mut example = load_example(&canvas, i).await?;
        example.render();
        draw_play_button(&canvas);
        let example = std::rc::Rc::new(std::cell::RefCell::new(example));
        let p = player.clone();
        let onclick = Closure::<dyn FnMut()>::new(move || {
            p.borrow_mut().toggle_play(example.clone());
        });
        canvas.set_onclick(Some(onclick.as_ref().unchecked_ref()));
        // Prevent cleanup of onclick (see https://rustwasm.github.io/wasm-bindgen/examples/closures.html):
        onclick.forget();
    }
    Ok(())
}

struct Player {
    frame: std::rc::Rc<std::cell::RefCell<PlayerFrame>>,
    frame_loop: std::rc::Rc<std::cell::RefCell<Option<Closure<dyn FnMut(f64)>>>>,
}

struct PlayerFrame {
    /// Animation frame id passed to cancelAnimationframe
    frame_id: Option<i32>,
    /// Current time stamp from animation frame
    t0: f64,
    /// Current example
    example_id: Option<u32>,
}

impl Player {
    fn new() -> Self {
        Self {
            frame: std::rc::Rc::new(std::cell::RefCell::new(PlayerFrame {
                frame_id: None,
                t0: 0.,
                example_id: None,
            })),
            frame_loop: std::rc::Rc::new(std::cell::RefCell::new(None)),
        }
    }
    fn start(&mut self, example: std::rc::Rc<std::cell::RefCell<Example>>) {
        let frame = self.frame.clone();
        let frame_loop = self.frame_loop.clone();
        *self.frame_loop.clone().borrow_mut() = Some(Closure::new(move |t| {
            example.borrow_mut().step((t - frame.borrow().t0) / 1000.);
            frame.replace(PlayerFrame {
                t0: t,
                example_id: Some(example.borrow().id),
                frame_id: Some(request_animation_frame(
                    frame_loop.borrow().as_ref().unwrap(),
                )),
            });
        }));
        // Make sure, that the start closure does not get
        // cleaned up before it is called in an animation
        // frame (the closure handles its reference counting
        // itself):
        let start_closure = std::rc::Rc::new(std::cell::RefCell::new(None));
        let frame = self.frame.clone();
        let frame_loop = self.frame_loop.clone();
        let start_closure_handle = start_closure.clone();
        *start_closure.clone().borrow_mut() = Some(Closure::new(move |t| {
            // Get current t, without evolving the simulation:
            frame.borrow_mut().t0 = t;
            // Kickstart the simulation
            // See https://developer.mozilla.org/en-US/docs/Web/API/Window/requestAnimationFrame
            request_animation_frame(frame_loop.borrow().as_ref().unwrap());
            // Now the start closure is no longer needed:
            start_closure_handle.borrow_mut().take();
        }));
        request_animation_frame(start_closure.borrow().as_ref().unwrap());
    }
    fn stop(&mut self) {
        if let Some(frame_id) = self.frame.borrow().frame_id {
            window()
                .cancel_animation_frame(frame_id)
                .expect("should register `cancelAnimationFrame` OK");
        }
        // Drop our handle to this closure so that it will get cleaned
        // up once we return.
        let _ = self.frame_loop.borrow_mut().take();
    }
    fn toggle_play(&mut self, example: std::rc::Rc<std::cell::RefCell<Example>>) {
        if self.frame_loop.borrow().is_some() {
            self.stop();
            if self.frame.borrow().example_id != Some(example.borrow().id) {
                self.start(example);
            }
        } else {
            self.start(example);
        }
    }
}

async fn load_example(canvas: &web_sys::HtmlCanvasElement, id: u32) -> Result<Example, JsValue> {
    let Some(example_path) = canvas.get_attribute("example") else {
        return Ok(Example::new(id, canvas, None)?);
    };
    let response =
        wasm_bindgen_futures::JsFuture::from(window().fetch_with_str(&example_path)).await?;
    let response: web_sys::Response = response.dyn_into()?;
    let json_str: String = wasm_bindgen_futures::JsFuture::from(response.text()?)
        .await?
        .as_string()
        .ok_or("Wrong result type")?;
    Ok(Example::new(id, canvas, Some(json_str))?)
}

fn request_animation_frame(f: &Closure<dyn FnMut(f64)>) -> i32 {
    window()
        .request_animation_frame(f.as_ref().unchecked_ref())
        .expect("should register `requestAnimationFrame` OK")
}

fn window() -> web_sys::Window {
    web_sys::window().expect("no global `window` exists")
}

fn document() -> web_sys::Document {
    window()
        .document()
        .expect("should have a document on window")
}

#[wasm_bindgen]
pub struct Example {
    pub id: u32,
    core: Kernel,
    ctx: graphics_js::Canvas,
}

#[wasm_bindgen]
impl Example {
    pub fn new(
        example_id: u32,
        canvas: &web_sys::HtmlCanvasElement,
        json_str: Option<String>,
    ) -> Result<Example, JsValue> {
        let (core, resize) = match json_str {
            None => (
                Kernel::new(canvas.width() as f64, canvas.height() as f64, 20),
                false,
            ),
            Some(s) => (
                serde_json::from_str(&s).map_err(|e| JsValue::from(&format!("{}", e)))?,
                true,
            ),
        };
        if resize {
            let (x_min, x_max, y_min, y_max) = core
                .boundary
                .boundary_box()
                .ok_or(JsValue::from("Invalid boundary"))?;
            canvas.set_width((x_max - x_min) as u32);
            canvas.set_height((y_max - y_min) as u32);
        }
        let ctx = graphics_js::Canvas(
            canvas
                .get_context("2d")
                .unwrap()
                .unwrap()
                .dyn_into::<web_sys::CanvasRenderingContext2d>()
                .unwrap(),
        );
        Ok(Self {
            id: example_id,
            core,
            ctx,
        })
    }
    pub fn render(&mut self) {
        self.core.render(&mut self.ctx);
    }
    pub fn step(&mut self, dt: f64) {
        self.core.step(dt);
        self.render();
    }
}

/// Draw a play button on a canvas
#[wasm_bindgen]
pub fn draw_play_button(canvas: &web_sys::HtmlCanvasElement) {
    let ctx = canvas
        .get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<web_sys::CanvasRenderingContext2d>()
        .unwrap();
    let (w, h) = (canvas.width() as f64, canvas.height() as f64);
    ctx.set_fill_style(&JsValue::from_str("rgba(255, 255, 255, 0.5)"));
    ctx.fill_rect(0., 0., w, h);
    ctx.begin_path();
    let (cx, cy) = (w / 2., h / 2.);
    let r = 0.4 * f64::min(w, h);
    ctx.set_line_width(0.2 * r);
    ctx.arc(cx, cy, r, 0., 2. * std::f64::consts::PI).unwrap();
    ctx.close_path();
    ctx.stroke();
    ctx.set_fill_style(&JsValue::from_str("black"));
    ctx.begin_path();
    let r_triangle = 0.7 * r;
    let sin_60 = 3.0_f64.sqrt() / 2.;
    let cos_60 = 0.5;
    ctx.move_to(cx - r_triangle * cos_60, cy + r_triangle * sin_60);
    ctx.line_to(cx - r_triangle * cos_60, cy - r_triangle * sin_60);
    ctx.line_to(cx + r_triangle, cy);
    ctx.close_path();
    ctx.fill();
}
